# Bug exemple project
StackOverflow post exemple. The post message is in this file : `stackOverflowPost.md`
or on [site](https://stackoverflow.com/questions/72171918).


## Config
My app is in Java 17 with Quarkus.

My default DB is a postgresql ([docker image](https://hub.docker.com/_/postgres/)). The second is a MariaDB but it is not called here (random values in `.properties` to compile and work).


## Launch
- Launch with Quarkus CLI : `quarkus dev`
- Lauch with Maven : `mvn compile quarkus:dev`
