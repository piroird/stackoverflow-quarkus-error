# Quarkus BlockingOperationNotAllowedException non-regular

Hello, I have a problem with panache and/or mutiny. My app use 2 datasources so I have to use imperative panache dependency for my default DB. I'm connected to the second with AgroalDataSource and I call my SQL request with JDBC.

The panache entity :
```
@Entity
@Table(name = "property_dictionnary")
public class DbPropertyDictionnary extends PanacheEntityBase {
    @Id @Column(name = "key")
    private String key;

    @Column(name = "value")
    private String value;

    // Getters

    public static DbPropertyDictionnary find(String key) {
        return find("key", key).singleResult();
    }
}
```

I access to these values with a mapper class :
```
public class DbPropertyMapper {
    private DbPropertyMapper() {}

    /**
     * Return the String value of the property key
     *
     * @param key the searched property
     * @return its value
     */
    public static Uni<String> get(String key) {
        return Uni.createFrom().item(() -> DbPropertyDictionnary.get(key).getValue());
    }

    /**
     * Return the Integer value of the property key
     *
     * @param key the searched property
     * @return its value
     */
    public static Uni<Integer> getInt(String key) {
        return get(key).onItem().transform(Integer::parseInt);
    }

    /**
     * Return the String value of the property key with an unblocking promise
     *
     * @param key the searched property
     * @return its value
     */
    public static Uni<String> getUnblocking(String key) {
        return Uni.createFrom()
                .item(() -> DbPropertyDictionnary.get(key).getValue())
                .runSubscriptionOn(Infrastructure.getDefaultWorkerPool());
    }
```

Now I have 3 request to execute on the second DB and these 3 queries need informations from my main DB. I write 3 methods who return my requests in an Uni<String\>.
```
@RequestScoped
public class MantisRepositoryRequestHelper {
    /**
     * Create the first SQL request (update status)
     *
     * @param idList IDs to update
     * @return the SQL request to execute
     */
    public Uni<String> firstRequest(Collection<Integer> idList) {
        Log.info("firstRequest");

        Uni<String> validateStatusUni =
                DbPropertyMapper.getUnblocking("VALIDATE_STATUS_ID")
                        .invoke(() -> Log.info("db is ok"));

        Uni<String> listJoinUni = Multi.createFrom().iterable(idList)
                .onItem().transform(id -> "" + id)
                .collect().with(Collectors.joining(","))
                .invoke(() -> Log.info("join is ok"));

        Log.info("launch");
        return Uni.join().all(validateStatusUni, listJoinUni).andCollectFailures()
                .onItem().transform(params -> {
                    Log.info("params");

                    String validateStatusId = params.get(0);
                    String stringList = params.get(1);

                    return String.format("""
                            UPDATE table1
                            SET status = %s
                            WHERE id IN (%s);
                            """,
                            validateStatusId, stringList);
                });
    }

    /**
     * Create the second SQL request (set SQL var)
     *
     * @return the SQL request to execute
     */
    public Uni<String> secondRequest() {
        Log.info("secondRequest");

        return DbPropertyMapper.get("VALIDATE_STATUS_USERNAME")
                .onItem().transform(validateUsername -> String.format("""
                        SET @userId = (SELECT id
                        FROM user_table
                        WHERE username = '%s');
                        """, validateUsername));
    }

    /**
     * Create the thrid SQL request (insert in history table)
     *
     * @param idList IDs updated
     * @return the SQL request to execute
     */
    public Uni<String> thirdRequest(Collection<Integer> idList) {
        Log.info("thirdRequest");

        StringBuilder querySb = new StringBuilder();
        String insertQueryHeader = """
                INSERT INTO history_table
                (user_id, bug_id, field_name, old_value, new_value, date_modified) VALUES
                """;

        return Uni.join().all(
                DbPropertyMapper.getInt("NEW_STATUS_ID"),
                DbPropertyMapper.getInt("VALIDATE_STATUS_ID"))
                .andCollectFailures()
                .onItem().transform(params -> {
                    Integer newTicketStatusId = params.get(0);
                    Integer validateStatusId = params.get(1);

                    int actualDateInt = (int) (new Date().getTime() / 1000);

                    // values construction
                    return idList.stream()
                            .map(id -> String.format(
                                    "(@userId, %d, 'status', %d, %d, %d)",
                                    id, newTicketStatusId, validateStatusId, actualDateInt))
                            .collect(Collectors.joining(",\n"));
                })
                .invoke(() -> querySb.append(insertQueryHeader)) // header
                .invoke(querySb::append) // values
                .invoke(() -> querySb.append(";")) // request ending
                .replaceWith(querySb::toString);
    }
}
```

My problem start here : the 3 methods are independent so I want to launch all and collect the results to execute them. If I do that (code below), the app crash with `BlockingOperationNotAllowedException`. But if I launch the first method alone and the 2 others after, the code work.
```
public Uni<String> willFailed() {
    Log.info("fail: start");
    return Uni.join().all(
            requestHelper.firstRequest(idList),
            requestHelper.secondRequest(),
            requestHelper.thirdRequest(idList)
    ).andCollectFailures()
            .invoke(sqlRequests -> {
                Log.info("fail: execute requests with JDBC");
                // otherDbRepository.validateIssue(sqlRequests);
            })
            .replaceWith("ok\n");
}

public Uni<String> willWork() {
    Log.info("work: start");

    return requestHelper.firstRequest(idList)
            .onItem().transformToUni(query -> Uni.join().all(
                    Uni.createFrom().item(query),
                    requestHelper.secondRequest(),
                    requestHelper.thirdRequest(idList)
            ).andCollectFailures())
            .invoke(sqlRequests -> {
                Log.info("work: execute requests with JDBC");
                // otherDbRepository.validateIssue(sqlRequests);
            })
            .replaceWith("ok\n");
}
```

How my method can block the thread when I'm joining 3 Uni but pass if it is alone ?

An exemple project is on my [gitlab](https://gitlab.com/piroird/stackoverflow-quarkus-error).
