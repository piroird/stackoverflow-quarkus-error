package com.swissknife.ticket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

/**
 * Class representing the DB property dictionnary
 */
@Entity
@Table(name = "property_dictionnary")
public class DbPropertyDictionnary extends PanacheEntityBase {
    @Id
    @Column(name = "key")
    private String key;

    @Column(name = "value")
    private String value;


    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    /**
     * Search the property
     *
     * @param key the searched property
     * @return the property object
     */
    public static DbPropertyDictionnary get(String key) {
        return find("key", key).singleResult();
    }
}

