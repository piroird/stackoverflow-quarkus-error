package com.swissknife.ticket.other_db;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;
import javax.enterprise.context.RequestScoped;
import com.swissknife.ticket.mapper.DbPropertyMapper;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

/**
 * My class who create SQL requests
 */
@RequestScoped
public class MantisRepositoryRequestHelper {
    /**
     * Create the first SQL request (update status)
     *
     * @param idList IDs to update
     * @return the SQL request to execute
     */
    public Uni<String> firstRequest(Collection<Integer> idList) {
        Log.info("firstRequest");

        Uni<String> validateStatusUni =
                DbPropertyMapper.getUnblocking("VALIDATE_STATUS_ID")
                        .invoke(() -> Log.info("db is ok"));

        Uni<String> listJoinUni = Multi.createFrom().iterable(idList)
                .onItem().transform(id -> "" + id)
                .collect().with(Collectors.joining(","))
                .invoke(() -> Log.info("join is ok"));

        Log.info("launch");
        return Uni.join().all(validateStatusUni, listJoinUni).andCollectFailures()
                .onItem().transform(params -> {
                    Log.info("params");

                    String validateStatusId = params.get(0);
                    String stringList = params.get(1);

                    return String.format("""
                            UPDATE table1
                            SET status = %s
                            WHERE id IN (%s);
                            """,
                            validateStatusId, stringList);
                });
    }

    /**
     * Create the second SQL request (set SQL var)
     *
     * @return the SQL request to execute
     */
    public Uni<String> secondRequest() {
        Log.info("secondRequest");

        return DbPropertyMapper.get("VALIDATE_STATUS_USERNAME")
                .onItem().transform(validateUsername -> String.format("""
                        SET @userId = (SELECT id
                        FROM user_table
                        WHERE username = '%s');
                        """, validateUsername));
    }

    /**
     * Create the thrid SQL request (insert in history table)
     *
     * @param idList IDs updated
     * @return the SQL request to execute
     */
    public Uni<String> thirdRequest(Collection<Integer> idList) {
        Log.info("thirdRequest");

        StringBuilder querySb = new StringBuilder();
        String insertQueryHeader = """
                INSERT INTO history_table
                (user_id, bug_id, field_name, old_value, new_value, date_modified) VALUES
                """;

        return Uni.join().all(
                DbPropertyMapper.getInt("NEW_STATUS_ID"),
                DbPropertyMapper.getInt("VALIDATE_STATUS_ID"))
                .andCollectFailures()
                .onItem().transform(params -> {
                    Integer newTicketStatusId = params.get(0);
                    Integer validateStatusId = params.get(1);

                    int actualDateInt = (int) (new Date().getTime() / 1000);

                    // values construction
                    return idList.stream()
                            .map(id -> String.format(
                                    "(@userId, %d, 'status', %d, %d, %d)",
                                    id, newTicketStatusId, validateStatusId, actualDateInt))
                            .collect(Collectors.joining(",\n"));
                })
                .invoke(() -> querySb.append(insertQueryHeader)) // header
                .invoke(querySb::append) // values
                .invoke(() -> querySb.append(";")) // request ending
                .replaceWith(querySb::toString);
    }
}
