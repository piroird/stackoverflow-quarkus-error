package com.swissknife.ticket.other_db;

import java.sql.Statement;
import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.SQLException;
import io.agroal.api.AgroalDataSource;
import io.quarkus.agroal.DataSource;


/**
 * Repository to the other DB
 */
@RequestScoped
public class OtherDbRepository {
    /**
     * The datasource to the other DB
     */
    @Inject
    @DataSource("other")
    AgroalDataSource dataSource;

    /**
     * Execution method
     * @param sqlRequests the list of request to execute on DB
     */
    public void executeRequests(Collection<String> sqlRequests) {
        // No transaction because tables have 'MyISAM' engine
        // which can't use transactions
        try (Connection dbConnection = dataSource.getConnection()) {
            try (Statement stmt = dbConnection.createStatement()) {
                for (String query : sqlRequests) {
                    stmt.addBatch(query);
                }
                // stmt.executeBatch();
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
