package com.swissknife.ticket.mapper;

import com.swissknife.ticket.model.DbPropertyDictionnary;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;

/**
 * Mapper for easier access to DB properties
 */
public class DbPropertyMapper {
    private DbPropertyMapper() {}

    /**
     * Return the String value of the property key
     *
     * @param key the searched property
     * @return its value
     */
    public static Uni<String> get(String key) {
        return Uni.createFrom().item(() -> DbPropertyDictionnary.get(key).getValue());
    }

    /**
     * Return the Integer value of the property key
     *
     * @param key the searched property
     * @return its value
     */
    public static Uni<Integer> getInt(String key) {
        return get(key).onItem().transform(Integer::parseInt);
    }

    /**
     * Return the String value of the property key with an unblocking promise
     *
     * @param key the searched property
     * @return its value
     */
    public static Uni<String> getUnblocking(String key) {
        return Uni.createFrom()
                .item(() -> DbPropertyDictionnary.get(key).getValue())
                .runSubscriptionOn(Infrastructure.getDefaultWorkerPool());
    }
}
