package com.swissknife.ticket.controller;

import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.swissknife.ticket.other_db.MantisRepositoryRequestHelper;
import com.swissknife.ticket.other_db.OtherDbRepository;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Uni;

@Path("/")
public class ReactiveProjeqtorRessource {
    /**
     * My class who create SQL requests
     */
    @Inject
    MantisRepositoryRequestHelper requestHelper;

    @Inject
    OtherDbRepository otherDbRepository;

    /**
     * An exemple list of ID
     */
    private Collection<Integer> idList = List.of(1, 2, 3);


    @GET
    @Path("/fail")
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> willFailed() {
        Log.info("fail: start");
        return Uni.join().all(
                requestHelper.firstRequest(idList),
                requestHelper.secondRequest(),
                requestHelper.thirdRequest(idList)
        ).andCollectFailures()
                .invoke(sqlRequests -> {
                    Log.info("fail: execute requests with JDBC");
                    // otherDbRepository.validateIssue(sqlRequests);
                })
                .replaceWith("ok\n");
    }

    @GET
    @Path("/work")
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> willWork() {
        Log.info("work: start");

        return requestHelper.firstRequest(idList)
                .onItem().transformToUni(query -> Uni.join().all(
                        Uni.createFrom().item(query),
                        requestHelper.secondRequest(),
                        requestHelper.thirdRequest(idList)
                ).andCollectFailures())
                .invoke(sqlRequests -> {
                    Log.info("work: execute requests with JDBC");
                    // otherDbRepository.validateIssue(sqlRequests);
                })
                .replaceWith("ok\n");
    }
}
